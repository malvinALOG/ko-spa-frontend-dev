define(["knockout", "jquery", "text!./login.html", "./ttam-0.3"], function(ko, jquery, loginTemplate, ttamjs){

    // assiging jquery import
    var $ = jquery;
    // assiging 23andme js library
    var TTandMe = ttamjs;


    function ApiTestViewModel(route) {
        this.message = ko.observable('Welcome to 23andMeAPIProject!');
    }


    function sendLoginCreds(creds) {
        $.ajax({
            url: "http://127.0.0.1:5000/login/?" + creds,
            type: "GET",
            crossDomain: true,
            success: function(response) {
                console.log(response);
            },
            error: function(xhr, status) {
                console.log("error: " + status);
            }
        });
    }
    
    function APIauth() {
        $.ajax({
            url: "http://127.0.0.1:5000/23ndMeLoginRedirect",
            success: function(response) {
                console.log(response);
            },
            error: function(xhr, status) {
                console.log(status);
                console.log(xhr);
            }
        });
    }


    $(document).ready(function(){

        $('#APIauth').click(function(event){
            window.location.href = "http://127.0.0.1:5000/23ndMeLoginRedirect";
        });

        $('.login-form').submit(function( event ){
            event.preventDefault();
            console.log(this);
            // this is the form element as turned into an object via JQuery selector method, so what's actually calling the anonymous function here with the serialize() method inside of it is the form-as-object, which is tied to the submit event
            var creds = $(this).serialize();
            sendLoginCreds(creds);
        });

        $("#TTandMeSignIn").click(function() {
            window.location.href = 'https://api.23andme.com/authorize/?redirect_uri=http://127.0.0.1:8080/&response_type=code&client_id=4c4b0078b9b3088d5f87f3df4f60a155&scope=basic%20names%20email%20ancestry%20family_tree%20relatives%20relatives:write%20analyses%20haplogroups%20report:all%20report:wellness.alcohol_flush%20report:wellness.caffeine_consumption%20report:wellness.deep_sleep%20report:wellness.lactose%20report:wellness.muscle_composition%20report:wellness.saturated_fat_and_weight%20report:wellness.sleep_movement%20genomes%20phenotypes:read:weight_g%20phenotypes:read:sex%20phenotypes:read:family_tree_url%20phenotypes:read:date_of_birth%20phenotypes:read:height_mm%20phenotypes:read:bd_pgen_patient_id';

            // returning false so no other events bubble up after redirection
            return false;
        });

    $("#get-accessions-button").click(function() {
        console.log("clicked");

        function callback(json){
            console.log(json);
        }

        // http://api.jquery.com/jquery.ajax/
        $.ajax({
            url: "https://api.23andme.com/3/accession/",
            // Data to be sent to the server. It is converted to a query string, if not already a string
            data: {
                format: 'jsonp'
           },
            error: function() {
                $('#info').html('<p>An error has occured with this request</p>');
            },

            // The type of data that you're expecting back from the server. If none is specified, jQuery will try to infer it based on the MIME type of the response
            dataType: 'json',

            //Override the callback function name in a JSONP request. This value will be used instead of 'callback' in the 'callback=?' part of the query string in the url.
            jsonp: "callback",
            success: function(data){
                console.log(data);
            },
            
            success: function(response){
                console.log(response);
            },

            type: 'GET',
            //crossDomain: true,
            headers: {'Authorization': 'Bearer bdb6fa21448a05a4a85b8b4f8099fab5'},
            //xhrFields: {
            //    withCredentials: true
            //}
        });
    });

    $("#testajax").click(function() {
        console.log("test ajax clicked");

    $.ajax({
        url: "http://json.sandboxed.guru/chapter9/cors.php",
        type: "GET",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
            console.log(response);
            $('#message').text(response.message);
        },
        error: function(xhr, status) {
            alert("error");
        }
    });

    });

    });






    //console.log("this is a test");

    ApiTestViewModel.prototype.doSomething = function() {
        this.message('you invoked doSomething() on the viewmodel.');
    };

    return { viewModel: ApiTestViewModel, template: loginTemplate};

});
