define(["knockout", "jquery", "bootstrap", "text!./marker.html"], function(ko, jquery, boostrap, markerTemplate){

    // assiging jquery import
    var $ = jquery;
    // assiging 23andme js library

    console.log("this is this in define: ");
    console.log(this)


    // KO view model
    // where is route being pass to this constructor?
    function markerViewModel(route) {

      $("#noresultsalert").hide();

      // initializing all tooltips on view
      $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      });

      console.log("INSIDE markerViewModel()");
      console.log("this is route: ");
      console.log(route);


      var param;

      // catches selection from dropdown menu
      $(document).ready(function(e){

        console.log("this is this right inside of doc.ready(): " + this);
         console.log(this);

        $('.search-panel .dropdown-menu').find('a').click(function(e) {
          console.log(e);
          e.preventDefault();

          console.log("this is this: " + this);
          console.log(typeof(this));
          console.log(this);
          console.log(this.toString());
          console.log(this.valueOf());
          console.log(this.constructor);

          // the value of this is the object created by passing the .seach-panel and .dropdown-menu
          // to the jquery selector
          param = $(this).attr("href").replace("#","");
          var concept = $(this).text();
          $('.search-panel span#search_concept').text(concept);
          //$('.input-group #search_param').val(param);
          console.log("this is the dropdown-menu selected option: " + param);

        });
      });

      // search button handler
      $("#searchBtn").click(function(e) {
        $("#noresultsalert").hide();
        self.KOMarkersResponseArray_geneName([]);
        self.KOMarkersResponseArray_accessionID([]);
        var searchMarkerQuery = $("input[name=searchTerm]").val();
        console.log(searchMarkerQuery);
        if (searchMarkerQuery != '') {
          console.log("not empty: " + searchMarkerQuery);
          // send search query and type of search to ajax call
          sendMarkerQuery(searchMarkerQuery, param);
        } else {
          console.log("empty string....");
          alert("Please enter a search term.");
        }

      });

      // marker AJAX query
      //$("#testaccession").click(function() {
      var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImZhZGVjMzYyZDRkNjQyYjQiLCJleHAiOjE0ODY2MTgxOTF9.YdJ-TAxxCJxQ__GfI2Ht99NDjAa1fc7Y-9q-IAb2KPU";


      var markersResponse;

      function sendMarkerQuery(searchMarkerQuery, param) {

        console.log("this in the AJAX call: ");
        console.log(this); // this is reset to Window object when this ajax call is hit

        // this is a handler for the OPTIONS request that the browser will make to the server as part of the preflight check (required due to the fact that this is a cross-site request with a non CORS-safelisted request-header (i.e. the Authorization header))
        beforeSend = function () {
          console.log("beforesend sent");
          xhr.setRequestHeader('Authorization', 'Bearer ' + token);
          console.log(xhr.status);
        };

    		var xhr = new XMLHttpRequest();

        if (param == 'accession_id') {
          console.log("Searching for accession_id....")
          var url = "http://127.0.0.1:5000/23andMe/api/v1.0/marker/?accession_id=" + searchMarkerQuery;
        } else if ( param == 'gene_name') {
          console.log("Searching for gene_name...")
          var url = "http://127.0.0.1:5000/23andMe/api/v1.0/marker/?gene_name=" + searchMarkerQuery;
        } else {
          //return "param error: param + " param;
          return "error: param";

        }

        xhr.open("GET",  url, true);
        //xhr.withCredentials = true;

        xhr.onreadystatechange = serverAlerts;

        // call ajax preflight request callback
        beforeSend();
    	  xhr.send();

        //var markersResponse;

        function serverAlerts() {
    			if (xhr.readyState === XMLHttpRequest.DONE) {
    				if (xhr.status === 200) {
              // store search query results
              markersResponse = JSON.parse(xhr.response);
              console.log("markersResponse: ");
              console.log(markersResponse);

              // checking for no results
              if (markersResponse["data"].length == 0) {
                $("#noresultsalert").show();
              } else {
                // parse response
                parseMarkers(markersResponse, param);
                //console.log(xhr.responseText);
                //$("#chromosomediv").show();
                //document.getElementById("jsoninsert4").textContent = xhr.responseText;
              }
    				} else {

    					console.log("There was a problem with the request....");
    					console.log(xhr.responseText);
    				}
    			}
    		}

      }; //end of ajax function


    // the very first time this viewmodel is called is when it is used as a constructor function
    // as part of the process of creating the viewmodel-as-object (tk exactly where and how, possibly via ko.components registration process in startup.js vs normally when you just pass a constructor function to ko.apply.bindings using new operator).
    //So initially, this == the viewmodel object that just got created. But very quickly, due to all the other code in here, such as the jquery selectors and ajax call, this becomes unbound from the viewmodel object and then this has the value of other objects, for example DOM elements or the global Window object. Essentially, this is equal to whatever object is in the current execution context.
    //That is why we need to save the first value of this that occurs during the initially activation of the constructor function that creates this view model to self, so that later, we can go back use the observables when we need to. The observables are after all properties of the viewmodel object, and thus, can only be accessed/are only defined in the context of the viewmodel.

    var self = this;

    console.log("this.toString(): ");
    console.log(this.toString());
    console.log("this.valueOf(): ");
    console.log(this.valueOf());
    console.log("this.constructor: ");
    console.log(this.constructor);
    console.log("this is self: ");
    console.log(self);

    //self.testKO = ko.observableArray([
    //  { name: "Bungle", id: "Bear" },
    //  { name: "George", id: "Hippo" },
    //  { name: "Zippy", id: "Unknown" }
    //]);

    self.KOMarkersResponseArray_geneName = ko.observableArray([]);
    self.KOMarkersResponseArray_accessionID = ko.observableArray([]);



    // array of chromosome objects
    var markersResults = [];

    // parses server response that is JSON string turned into an object
    function parseMarkers(markersResponse, param) {

      if (param == 'gene_name') {
        self.KOMarkersResponseArray_geneName([]);
        self.KOMarkersResponseArray_accessionID([]);
        markersResults = [];

        for (var obj in markersResponse["data"]) {
          //self.KOMarkersResponseArray([]);
          var entry = {};
          var responseObj = markersResponse["data"][obj];
          console.log(responseObj);
          entry["id"] = markersResponse["data"][obj].id;
          entry["accession_id"] = markersResponse["data"][obj].accession_id;
          entry["start"] = markersResponse["data"][obj].start;
          entry["end"] = markersResponse["data"][obj].end;
          // for variants
          entry["allele_0"] = markersResponse["data"][obj].variants[0].allele;
          entry["allele_1"] = markersResponse["data"][obj].variants[1].allele;
          markersResults.push(entry);
        }
        self.KOMarkersResponseArray_geneName(markersResults);

    } else { // else here is accession_id param was searched for
        self.KOMarkersResponseArray_geneName([]);
        self.KOMarkersResponseArray_accessionID([]);
        markersResults = [];

        for (var obj in markersResponse["data"]) {
          //self.KOMarkersResponseArray([]);
          var entry = {};
          var responseObj = markersResponse["data"][obj];
          console.log(responseObj);
          entry["id"] = markersResponse["data"][obj].id;
          entry["accession_id"] = markersResponse["data"][obj].accession_id;
          entry["start"] = markersResponse["data"][obj].start;
          entry["end"] = markersResponse["data"][obj].end;
          entry["gene_names"] = markersResponse["data"][obj].gene_names[0];
          markersResults.push(entry);
        }
        self.KOMarkersResponseArray_accessionID(markersResults);

    }

      console.log("markersResults: ");
      console.log(markersResults);

      //self.loadKOMarkersResponseArray(markersResults);
      //self.KOMarkersResponseArray(markersResults);
    }


    } // end of viewmodel


    return { viewModel: markerViewModel, template: markerTemplate };


});
