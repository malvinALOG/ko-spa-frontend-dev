define(["knockout", "jquery", "bootstrap", "text!./accession.html"], function(ko, jquery, boostrap, accessionTemplate){

    // assiging jquery import
    var $ = jquery;
    // assiging 23andme js library

    function accessionViewModel(route) {

      $(document).ready(function() {
        $("a.tooltipLink").tooltip();
      });

      // hide all the chromosome images upon page load
      $("#chromosomediv").hide();

      this.numberOfClicks = ko.observable(0);
      this.chromosomeImageId = ko.observable();

      this.chromosome = ko.observable();
      this.chromosome_id = ko.observable();
      this.chromosome_length = ko.observable();

      //for XY chromosome
      this.Xchromosome_id = ko.observable();
      this.Xchromosome_length = ko.observable();
      this.Ychromosome_id = ko.observable();
      this.Ychromosome_length = ko.observable();

      incrementClickCounter = function(data, event) {
            var previousCount = this.numberOfClicks();
            this.numberOfClicks(previousCount + 1);
            //console.log(this.numberOfClicks);

            var chromoImageId = event.target.id;
            this.chromosomeImageId(chromoImageId);

            // splitting off number from element id of image and converting to integer to pull selected chromosome object from chromosomes array that contains server response objects, decrementing by 1 to avoid off-by-one error
            //var chromObj = getChromData(parseInt(chromoImageId.split("_")[1]) - 1);

            // checking for chromosome 23 selection, in UI clicking on 23 image will display both X and Y data in modal
            if (chromoImageId == "chromosome_23") {
              // to get X object at index 22 in chromosomes array
              var chromObj = getChromData(22);
              this.Xchromosome_id(chromObj["id"]);
              this.Xchromosome_length(chromObj["length"]);
              // to get Y object at index 23 in chromosomes array
              var chromObj = getChromData(23);
              this.Ychromosome_id(chromObj["id"]);
              this.Ychromosome_length(chromObj["length"]);

            } else { //all other chromosomes
              var chromObj = getChromData(parseInt(chromoImageId.split("_")[1]) - 1);
              // using bracket notation to stay consistent
              this.chromosome(chromObj["chromosome"]);
              this.chromosome_id(chromObj["id"]);
              this.chromosome_length(chromObj["length"]);
            }

        }


      this.message = ko.observable('Welcome to 23andMeAPIProject!');

      var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImZhZGVjMzYyZDRkNjQyYjQiLCJleHAiOjE0ODY2MTgxOTF9.YdJ-TAxxCJxQ__GfI2Ht99NDjAa1fc7Y-9q-IAb2KPU";

      $("#testaccession").click(function() {
        console.log("clicked");
        // this is a handler for the OPTIONS request that the browser will make to the server as part of the preflight check (required due to the fact that this is a cross-site request with a non CORS-safelisted request-header (i.e. the Authorization header))
        beforeSend = function () {
          console.log("beforesend sent");
          xhr.setRequestHeader('Authorization', 'Bearer ' + token);
          console.log(xhr.status);
        };

    		var xhr = new XMLHttpRequest();
        xhr.open("GET",  "http://127.0.0.1:5000/23andMe/api/v1.0/accession/", true);
        //xhr.withCredentials = true;

        xhr.onreadystatechange = serverAlerts;

        beforeSend();
    	  xhr.send();

        var accessions;

    		function serverAlerts() {
    			if (xhr.readyState === XMLHttpRequest.DONE) {
    				if (xhr.status === 200) {
              accessions = JSON.parse(xhr.response);

              console.log(typeof(accessions));
              console.log("server response: " + accessions["data"]);

              parseAccessions(accessions);
              //console.log(xhr.responseText);
              $("#chromosomediv").show();
              document.getElementById("jsoninsert4").textContent = xhr.responseText;
    				} else {
    					console.log("There was a problem with the request....");
    					console.log(xhr.responseText);
    				}
    			}
    		}
    	});

    // array of chromosome objects
    var chromosomes = [];

    // parses server response that is JSON string into an object
    function parseAccessions(accessions) {
      for (var chromosome in accessions["data"]) {
        //var entry = new Object();
        //entry[chromosome] = accessions["data"][chromosome];
        //console.log(accessions["data"][chromosome]);
        //chromosomes.push(entry);
        chromosomes.push(accessions["data"][chromosome])
      }
      console.log("this is chromosomes array: ");
      console.log(chromosomes);
    }

    // getting chromosome object in array using split id from image element user clicks
    function getChromData(imageElementId) {
      return chromosomes[imageElementId];
    }

    //console.log("this is a test");
    accessionViewModel.prototype.doSomething = function() {
        this.message('you invoked doSomething() on the viewmodel.');
    };

  }
    return { viewModel: accessionViewModel, template: accessionTemplate };


});
