define(["knockout", "jquery", "text!./apitest.html", "./ttam-0.3"], function(ko, jquery, apitestTemplate, ttamjs){

    // assiging jquery import
    var $ = jquery;
    // assiging 23andme js library
    var TTandMe = ttamjs;


    function ApiTestViewModel(route) {
        this.message = ko.observable('Welcome to 23andMeAPIProject!');
    }

    $(document).ready(function(){

        $("#apiAuthCallTest").click(function() {
            $.ajax({
                url: "http://127.0.0.1:5000/testjwt/",
                type: "GET",
                beforeSend: function(xhr){
                    xhr.withCredentials = true;
                },
                crossDomain: true,
                success: function(response) {
                    console.log(response);
                },
                error: function(xhr, status) {
                    console.log("ERROR: " + status);
                }

            });
        });
    });

    //console.log("this is a test");

    ApiTestViewModel.prototype.doSomething = function() {
        this.message('you invoked doSomething() on the viewmodel.');
    };

    return { viewModel: ApiTestViewModel, template: apitestTemplate};

});
