1. The dist directory is initially created by running ```$ gulp```. It is this directory that would actually be hosted, as all the components and other js files/libraries are compiled into the scripts.js file here. 

2. All KO components are in src/components

3. The login view is dervied from the component called 23andMe-OAuth, which just provides a button the user clicks to be redirected to a 3rd party oauth via my Python API. After successful authentication, 3rd party oauth provider redirects back to Python API, which then creates temporary token and finally returns client back to home page of the KO SPA with token embedded in cookie.

